text1 = "ABC"
text2 = "123"
print(text1 + text2) #Łączenie dwoch łańcuchów znaków
print(text2 * 3) #Replikuje zawartość zmiennej
text = """First line
Second Line
Third Line 
"""
print(text) #Tekst wieloliniowy dzięki potrójnemu cudzysłowiowi
text = "aBcD"
print(text.capitalize()) #Przekształca tekst tak aby zaczynał sie z wielkiej litery
print(text.upper()) #Ustawia wszystkie znaki na wielkie litery
print(text.lower()) #Ustawia wszystkie znaki na małe litery
text = "aBcD eFgH"
print(text.title())