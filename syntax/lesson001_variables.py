print("Hello world")
name = "Marcin" #zmienna nie wymaga deklarowania typu
print("Hello", name) #print z wieloma argumentami pozwala oddzielać tekst dodatkową spacją
x = name
x = 5
print(x) # zmienna może być zmieniać nie tylko wartości ale również typ
print(type(x)) #metoda zwracająca typ zmiennej

a = 1; b=2; c=3 #Deklaracja wielu zmiennych w jednej linii za pomocą średnika
print(a,b,c)
a,b,c = 1,2,3 #Deklaracja wielu zmiennych za pomocą przecinka
print(a,b,c)