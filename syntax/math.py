x = 5
y = 10
print(type(x))
print(type(y))
print(x+y)
print(x-y)
print(x*y)
print(x/y)
print(type(x/y)) #Dzielenie dwóch liczb typu int zwraca float
print(x%y) #Operacja modulo (reszta z dzielenia)
print(x**3) #Skrócony zapis potęgowania
print(x//2) #Dzielenie całkowitoliczbowe
#Operacje matematyczne z szybkim przypisaniem do zmiennej
x += 2
x -= 2
x *= 2
x /= 2
print(abs(-10)) #Wbudowana metoda wartości bezwzględnej
print(pow(10,3)) #Wbudowana metoda na potęgowanie
